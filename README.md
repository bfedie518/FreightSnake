# Freight Snake
A simple game of snake with forklifts as the snake and pallets as the food.

Released under the MIT license.

Download latest build from [itch.io](https://bfedie518.itch.io/freight-snake)
