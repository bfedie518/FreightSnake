class_name Head
extends BodyPart


signal end_game
signal scored

enum Dir {LEFT = 1, RIGHT = 2, UP = 3, DOWN = 4}

export var move_speed: int = 44

var _direction: int = Dir.UP
var _last_moved: int = Dir.UP


func _process(_delta) -> void:
	if Input.is_action_just_pressed("move_left"):
		Input.action_release("move_left")
		if self.follower == null or _last_moved != Dir.RIGHT:
			_direction = Dir.LEFT
	if Input.is_action_just_pressed("move_right"):
		Input.action_release("move_right")
		if self.follower == null or _last_moved != Dir.LEFT:
			_direction = Dir.RIGHT
	if Input.is_action_just_pressed("move_up"):
		Input.action_release("move_up")
		if self.follower == null or _last_moved != Dir.DOWN:
			_direction = Dir.UP
	if Input.is_action_just_pressed("move_down"):
		Input.action_release("move_down")
		if self.follower == null or _last_moved != Dir.UP:
			_direction = Dir.DOWN


func _on_MoveTimer_timeout() -> void:
	if self.follower != null:
		self.follower.move(self.position, self.rotation)
	match _direction:
		Dir.LEFT:
			self.position.x -= move_speed
			self.rotation = PI * 1.5
		Dir.RIGHT:
			self.position.x += move_speed
			self.rotation = PI * .5
		Dir.UP:
			self.position.y -= move_speed
			self.rotation = 0
		Dir.DOWN:
			self.position.y += move_speed
			self.rotation = PI
	_last_moved = _direction


func _on_Head_area_entered(area: Area2D) -> void:
	if area.is_in_group("food"):
		area.queue_free()
		.emit_signal("scored")
	elif area.is_in_group("walls") or (area.is_in_group("bodies") and area != self.follower):
		(.get_node("MoveTimer") as Timer).stop()
		.emit_signal("end_game")
