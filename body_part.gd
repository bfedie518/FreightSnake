class_name BodyPart
extends Area2D

var follower: Body setget add_follower

func add_follower(new_follower: Body) -> void:
	if follower == null:
		follower = new_follower
		follower.position = position
		follower.rotation = rotation
	else:
		follower.follower = new_follower
