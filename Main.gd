extends Node


export var food_scene: PackedScene
export var body_scene: PackedScene

var _score: int = 0

onready var head := get_node("Head") as Head
onready var game_over_screen := get_node("UI/GameOver") as Control

func _ready() -> void:
	game_over_screen.hide()
	randomize()
	generate_food()


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept") and game_over_screen.visible:
		# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()


func generate_food() -> void:
	var food := food_scene.instance() as Node2D
	var snake_pos:= []

	for member in get_tree().get_nodes_in_group("bodies"):
		snake_pos.append((member as Node2D).position)

	var food_pos: Vector2 #:= Vector2((randi() % 9 + 2) * 32 -12, (randi() % 17 + 2) * 32 - 16)
	while food_pos == Vector2.ZERO or food_pos == head.position or snake_pos.has(food_pos):
		food_pos = Vector2((randi() % 9 + 2) * 32 + -12, (randi() % 17 + 2) * 32 - 16)
	food.position = food_pos
	food.rotation = (randi() % 4) * PI / 2

	call_deferred("add_child", food)


func add_follower() -> void:
	var new_follower := body_scene.instance() as Body
	head.follower = new_follower
	call_deferred("add_child", new_follower)


func _on_Head_end_game() -> void:
	game_over_screen.show()


func _on_Head_scored() -> void:
	add_follower()
	generate_food()
	_score += 1
	($UI/ScoreLabel as Label).text = str(_score)


func _on_UpArrow_input_event(_viewport, event: InputEvent, _shape_idx) -> void:
	if event is InputEventMouseButton and event.pressed:
		Input.action_press("move_up")


func _on_DownArrow_input_event(_viewport, event: InputEvent, _shape_idx) -> void:
	if event is InputEventMouseButton and event.pressed:
		Input.action_press("move_down")


func _on_LeftArrow_input_event(_viewport, event: InputEvent, _shape_idx) -> void:
	if event is InputEventMouseButton and event.pressed:
		Input.action_press("move_left")


func _on_RightArrow_input_event(_viewport, event: InputEvent, _shape_idx) -> void:
	if event is InputEventMouseButton and event.pressed:
		Input.action_press("move_right")


func _on_RestartButton_pressed() -> void:
	# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()
