class_name Body
extends BodyPart


func move(new_pos: Vector2, new_rotation: float) -> void:
	if self.follower != null:
		self.follower.move(self.position, self.rotation)
	self.position = new_pos
	self.rotation = new_rotation

